
<?php
  // $format = have_posts() ? get_post_format() : false;
  // get_template_part('templates/content', $format);
?>
<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class(); ?>>
    <header>
      <a href="<?php the_permalink(); ?>">
        <h1 class="entry-title"><?php the_title(); ?></h1>
      </a>
      <?php get_template_part('templates/entry-meta'); ?>
    </header>
    <div class="entry-content">
      <?php the_excerpt(); ?>
    </div>
  </article>
<?php endwhile; ?>