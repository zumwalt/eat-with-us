
<div class="not-found">

  <h1>404</h1>
  <h2>The page you're looking for doesn't exist.</h2>
  <p><b>404: Page not found.</b> Double check the URL or head back to <a href="<?php echo home_url(); ?>">the homepage</a>.</p>

</div>


