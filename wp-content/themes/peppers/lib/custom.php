<?php 
// Custom Functions

// Remove the admin bar from the front end
// add_filter( 'show_admin_bar', '__return_false' );


// Remove the version number of WP
// Warning - this info is also available in the readme.html file in your root directory - delete this file!
remove_action('wp_head', 'wp_generator');


// Obscure login screen error messages
function wpfme_login_obscure(){ return '<strong>Error</strong>: Either the username or password is incorrect.';}
add_filter( 'login_errors', 'wpfme_login_obscure' );


// Disable the theme / plugin text editor in Admin
define('DISALLOW_FILE_EDIT', true);

    register_sidebar(array(
		'name' => __('Header Area'),
		'id'   => 'header-area',
        'before_widget' => '<div class="header-box">',
		'after_widget' => '</div>',
		'before_title' => '<h2>',
        'after_title' => '</h2>'
    ));

?>