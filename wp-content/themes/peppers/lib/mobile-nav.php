<?php
/**
 * Custom Menu Walker for Responsive Menus
 *
 * Creates a <select> menu instead of the default
 * unordered list menus.
 *
 **/

class Walker_Nav_Menu_Dropdown extends Walker_Nav_Menu{
        function start_lvl(&$output, $depth){
            $indent = str_repeat("\t", $depth);
            }

        function end_lvl(&$output, $depth){
            $indent = str_repeat("\t", $depth);
        }

        function start_el(&$output, $item, $depth, $args){
            $item->title = str_repeat("&nbsp;- ", $depth).$item->title;

            parent::start_el($output, $item, $depth, $args);

            $href =! empty( $item->url ) ? ' value="'   . esc_attr( $item->url ) .'"' : '#';

            $output = str_replace('<li', '<option '.$href, $output);
        }

        function end_el(&$output, $item, $depth){
            $output .= "</option>\n"; // replace closing </li> with the option tag
        }
}
?>