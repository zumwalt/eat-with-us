// Author: Lindley White


$(window).load(function() {
 // $('.pager-house').appendTo($('#rev_slider_1_1_wrapper'));
  //$('.tp-bullets').prependTo('#pager-here');

  var rtheight = $('.footer.rt').height(),
      ltheight = $('.footer.lt').height();

  if(rtheight > ltheight) {
    $('.footer.lt').height(rtheight);
  } else {
    $('.footer.rt').height(ltheight);
  }

  //====================
  // Dropdown Select Navigation
  //====================

  $("#secondary-nav select").on('change', function() {
      window.location = $(this).find("option:selected").val();
  });

  //$("#secondary-nav select option.active").prop("selected", true);


  $(window).resize(function(){
    var rtheight = $('.footer.rt').height(),
          ltheight = $('.footer.lt').height();

      if(rtheight > ltheight) {
        $('.footer.lt').height(rtheight);
      } else {
        $('.footer.rt').height(ltheight);
      }
  });

  // Promo area vertical center
  // -------------------------

  $('.promo').each(function(){
    var parentHeight = $(this).parents('.first-header').height();
    $(this).css("top", (parentHeight - $(this).outerHeight())/2);
  });

  // Form Hinting
  // -------------------------

  $('.hinting .input').each(function(){
    var input = $('input', this),
        label = $('label', this);

    input.on("focus", function(){
      if( $(this).val().length === 0 ) {
        label.css("opacity","0.5");
      }
    });

    label.on("click", function(){
      input.focus();
    });

    input.keyup(function(){
      label.css("opacity",0);
      if( $(this).val().length === 0 ) {
        label.css("opacity","0.5");
      }
    });

    input.on("blur", function(){
      if( $(this).val().length === 0 ) {
        label.css("opacity","1");
      }
    });


  });

  
  //Grill Slider Fix
    var fixGrillSlider = function() {
      if( $(window).width() > 1280) {
        console.log()
        var sHeight = $('body.grill #rev_slider_1_1').height();
        var addPager = sHeight + 64;
        var margTopPager = sHeight - 167;
        $('body.grill .pager-house').css('margin-top', margTopPager);
        $('body.grill .slider').css('height', addPager);
        $('body.grill .tp-bullets').css('top', sHeight + 15);
        $('body.grill .mask').css('height', sHeight);
        
      }else if ($(window).width() > 1024  && $(window).width() < 1280) {
        var height = $(' body.grill #rev_slider_1_1').height();
        $('body.grill .slider').css('height', height);
        var margTopPager = height - 167;
        $('body.grill .pager-house').css('margin-top', margTopPager);
        var addPager = height + 64;
        $('body.grill .slider').css('height', addPager);
        $('body.grill .tp-bullets').css('top', height + 15);
      }else {
        var height = $(' body.grill .slider').height();
        var addPager = height + 64;
        $('body.grill .slider').css('height', addPager);
        $('body.grill .pager-house').css('margin-top', '-64px');
      }
    }

  fixGrillSlider();
  

});


jQuery(function($) {
    $("#menu-carousel").flexslider({
    animation: "slide",
    animationLoop: true,
    slideshow: false,
    itemWidth: 164,
    itemMargin: 10,
    controlNav: false,
    maxItems: 5
    });


    $('.slider').each(function(){
      var windoww = $(window).width(),
          viewport = 1000,
          offset = (windoww - viewport)/2 + viewport,
          maskl = $('.mask.left', this),
          maskr = $('.mask.right',this);


      $('.mask', this).height($('.slider').height()).width(windoww);

      maskl.css('right', offset);
      maskr.css('left', offset);

    });

    $(window).resize(function(){

      $('.slider').each(function(){
        var windoww = $(window).width(),
            viewport = 1000,
            offset = (windoww - viewport)/2 + viewport,
            width = (windoww - viewport)/2,
            maskl = $('.mask.left', this),
            maskr = $('.mask.right',this);

        $('.mask', this).height($('.slider').height()).width(width);

        maskl.css('right', offset);
        maskr.css('left', offset);

        
      });
    });

    $('#jumpMenu').bind('change', function () { // bind change event to select
        var url = $(this).val(); // get selected value
        if (url != '') { // require a URL
            window.location = url; // redirect
        }
        return false;
    });

    

        

});






