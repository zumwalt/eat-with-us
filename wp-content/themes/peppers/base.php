<?php get_template_part('templates/head'); ?>
<body <?php body_class(); ?>>

  <?php if($blog_id == 1) {

    get_template_part('templates/ewu', 'landing');

  } else { ?>

    <?php
      do_action('get_header');
      
      
        get_template_part('templates/header');
      
    ?>

    <?php if(is_front_page()) : 

        get_template_part('templates/content', 'mobile-flexible');
        get_template_part('templates/content', 'slider');
        get_template_part('templates/content', 'flexible');

    ?>
    <?php elseif(get_post_type() == 'location')  :
        get_template_part('templates/content', 'location');
    ?>
    
    <?php else: ?>

    <div class="wrap container">
      <div class="content row">
        <div class="large-12 columns">

          <?php if(is_single() || is_blog()) : ?>
          
            <div class="row">
              <div class="main large-9 small-12 columns" id="main-content">
                  <?php include roots_template_path(); ?>
                </div><!-- /.main -->
              <div class="large-3 small-12 columns" id="sidebar_right">
                <?php get_template_part('templates/sidebar', 'right'); ?>
              </div>
            </div>

          <?php else : ?>
            
            <div class="row">

              <?php if (roots_display_sidebar()) : ?>
                <div class="sidebar <?php echo roots_sidebar_class(); ?>" id="sidebar">
                  <?php include roots_sidebar_path(); ?>
                </div><!-- /.sidebar -->
              <?php endif; ?>

              <div class="main <?php echo roots_main_class(); ?>" >
                <?php include roots_template_path(); ?>
              </div><!-- /.main -->
            </div>

          <?php endif; ?>
        
      </div><!-- /.content -->
    </div><!-- /.wrap -->
  </div>
  <?php endif; ?>
    <?php get_template_part('templates/footer'); ?>

<?php } // if is blog 1 ?>

</body>
</html>
