<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <title><?php wp_title('|', true, 'right'); ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <?php wp_head(); ?>
  
  <?php global $blog_id;
  if($blog_id == 4) { ?>
    <script type="text/javascript" src="//use.typekit.net/yaf1wtr.js"></script>
    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
  <?php } elseif($blog_id == 5) { ?>
    <script type="text/javascript" src="//use.typekit.net/mau0knr.js"></script>
    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
  <?php } elseif($blog_id == 6) { ?>
    <script type="text/javascript" src="//use.typekit.net/uew4rws.js"></script>
    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
  <?php } ?>


  <link rel="alternate" type="application/rss+xml" title="<?php echo get_bloginfo('name'); ?> Feed" href="<?php echo home_url(); ?>/feed/">
</head>
