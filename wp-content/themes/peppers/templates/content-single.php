<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class(); ?>>
    <header>
      <h1 class="entry-title"><?php the_title(); ?></h1>
      <?php get_template_part('templates/entry-meta'); ?>
    </header>
    <div class="entry-content">
      <?php the_content(); ?>
    </div>
    <footer>
      <?php wp_link_pages(array('before' => '<nav class="page-nav"><p>' . __('Pages:', 'roots'), 'after' => '</p></nav>')); ?>
      <?php the_tags('<ul class="entry-tags"><li>','</li><li>','</li></ul>'); ?>
       <div class="navigation">
        <?php
        $next_post = get_next_post();
        $previous_post = get_previous_post();

        if (!empty($previous_post)):
        ?>
          <a href="<?php echo get_permalink($previous_post->ID); ?>">&lsaquo; Previous post</a>
         
        <?php
          endif;
          if(!empty($next_post) && !empty($previous_post)): echo '<span class="divider">|</span>'; endif;
          if (!empty($next_post)):
        ?>
          <a href="<?php echo get_permalink($next_post->ID); ?>">Next post &rsaquo;</a>
                                       
        <?php endif; ?>
      </div>

        <div class="share">
          <div id="twitter" data-url="<?php the_permalink(); ?>" data-text="<?php the_title(); ?>" data-title="Tweet"></div>
          <div id="facebook" data-url="<?php the_permalink(); ?>" data-text="<?php the_title(); ?>" data-title="Like"></div>
          
          <script>
            $(document).ready(function(){
              $('#twitter').sharrre({
                share: {
                  twitter: true
                },
                enableHover: false,
                enableTracking: true,
                enableCounter: false,
                click: function(api, options){
                  api.simulateClick();
                  api.openPopup('twitter');
                }
              });
              $('#facebook').sharrre({
                share: {
                  facebook: true
                },
                enableHover: false,
                enableTracking: true,
                enableCounter: false,
                click: function(api, options){
                  api.simulateClick();
                  api.openPopup('facebook');
                }
              });
            });
          </script>

        </div>
      

    </footer>
  </article>
<?php endwhile; ?>
