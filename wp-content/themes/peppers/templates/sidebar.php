
<?php dynamic_sidebar('sidebar-primary'); ?>

<?php 
	$sidebarTop = get_field('sidebar_top_image', 'option');
	$sidebarBottom = get_field('sidebar_bottom_image', 'option');
?>

<div class="row">
     <div class="promo_1_wrap large-12 small-5 columns">
             <a href="<?php the_field('sidebar_top_link', 'option'); ?>"><img src="<?php echo $sidebarTop ?>" /></a>
      </div>


     <div class="promo_2_wrap large-12 small-6 columns">
             <a href="<?php the_field('sidebar_bottom_link', 'option'); ?>"><img src="<?php echo $sidebarBottom ?>" /></a>
      </div>
</div>