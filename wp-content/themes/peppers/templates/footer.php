<footer class="mobile-footer full-width small-12 columns  show-for-small">
	<nav class="mobile-simple">

		<?php
          if (has_nav_menu('mobile_footer')) :
            wp_nav_menu(array('theme_location' => 'mobile_footer', 'menu_class' => 'nav small-12 columns'));
          endif;
        ?>
    </nav>


    <div id="secondary-nav">
	<?php
    	wp_nav_menu(array(
    	  'container' => false,
    	  'menu_id' => 'nav',
		  'menu' => 'Primary Navigation', // your theme location here
		  'walker'         => new Walker_Nav_Menu_Dropdown(),
		  'items_wrap'     => '<select>%3$s</select>',
		));
	?>	
	<p>© 2013 Eat With Us All Rights Reserved.
This site is optimized for mobile devices. 
To view the full site, visit 
<?php 
$site = site_url(); 
$site_url = preg_replace('/http:\/\//', '', $site);
echo $site_url;
?> on a computer or tablet.</p>
    </div>
</footer>

<footer class="site-footer full-width" role="contentinfo">

	

	<div class="footer half-width lt">
		
		<div class="container"><div class="left split">
					<div class="brand-image">
						<a href="/">
							<?php
							global $blog_id;
							if($blog_id == 4) { ?>
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/peppers-logo-white.png">
							<?php } elseif ($blog_id == 5) { ?>
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/grill/grill-logo-white.png">
							<?php } elseif ($blog_id == 6) { ?>
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/harveys/harveys-logo-white.png">
							<?php } ?>
						</a>
					</div>
					<div class="social">
						<ul class="social-ul large-block-grid-3">
							<li><a class="ss-icon ss-social-circle" href="<?php the_field('twitter_link', 'option'); ?>" target="_blank">Twitter</a></li>
							<li><a class="ss-icon ss-social-circle" href="<?php the_field('facebook_link', 'option'); ?>" target="_blank">Facebook</a></li>
							<li><a class="ss-icon ss-social-circle" href="<?php the_field('email_link', 'option'); ?>" target="_blank">Email</a></li>
						</ul>
					</div>

				</div>
		
				<div class="right split">
					<div class="section-header">
						<h2>Links</h2>
					</div>
					<?php wp_nav_menu(array(
						"menu" => "Footer Menu",
						"menu_class" => "large-block-grid-2"
					)); ?>
					
					<p class="copyright">
						&copy; 2013 Eat With Us
						All Rights Reserved
					</p>

				</div>	

			</div>	

	</div>

	<div class="footer half-width rt">
		<div class="container">
			<div class="section-header">
				<h2>Recent News</h2>
				<h5><a href="<?php echo home_url(); ?>/news/">See All</a></h5>
			</div>
			<ul class="recent-news"><?php
	 					
						
			global $post;
				$myposts = get_posts('numberposts=2' );
				foreach($myposts as $post) :
					setup_postdata($post);?>
		            
		            
				<li>
					<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
	        <time>Posted on <?php the_time('M j Y'); ?></time>
	       	<p><?php echo excerpt(15); ?></p>
	        <a class="read-more" href="<?php the_permalink(); ?>">Read More</a>
	      </li>
				<?php endforeach; ?>
				</ul> 
			</div>
	</div>
  
</footer>
<footer class="ewu-footer">
	<ul class="links">
		<li class="ewu-brand"><a  href="http://www.eatwithus.com/" target="_blank">Eat With Us</a></li>
		<li><a href="<?php echo home_url(); ?>/eat-with-us-about/">About "Eat With Us"</a></li>
		<li><a href="<?php echo home_url(); ?>/eat-with-us-contact/">Contact Corporate</a></li>
		<li><a href="<?php echo home_url(); ?>/eat-with-us-careers/">Career Opportunities</a></li>
	</ul>
</footer>

<?php wp_footer(); ?>
