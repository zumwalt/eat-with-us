<?php 

	// Subpage Navigation
	$parent = $post->post_parent;
	$children = get_pages('child_of='.$post->ID);
	$siblings = get_pages('child_of='.$parent);

  if( count( $children ) != 0 ) {

		echo '<ul class="about-sub page-sub">'; ?>

		<li class="parent active">
			<a href="<?php the_permalink($parent); ?>"><?php the_title(); ?></a>
		</li>

	 	<?php $args = array(
	 			"child_of" => $post->ID,
	 			"depth" => 1,
	 			'title_li' => ''
	 		);
	 	$sidebar_nav = wp_list_pages($args);
	 	echo "</ul>";

	} elseif(count($children) == 0 && count($siblings) != 0) {

	  echo '<ul class="about-sub page-sub">'; ?>

		<li class="parent">
			<a href="<?php echo get_permalink($post->post_parent);?>"><?php echo get_the_title($post->post_parent);?></a>
		</li>

	 	<?php $args = array(
	 			"child_of" => $parent,
	 			"depth" => 1,
	 			'title_li' => ''
	 		);
	 	$sidebar_nav = wp_list_pages($args);
	 	echo "</ul>";

	}

?>