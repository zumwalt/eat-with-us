<div class="slider">
  <?php
   $rev_slider = get_field('revolution_slider_name');
   putRevSlider($rev_slider);

   ?>
  <div class="mask left"></div>
  <div class="mask right"></div>
</div>

<div class="pager-house container">
	<div class="row">
		<div id="pager-here" class="large-12 columns">

		</div>
	</div>
</div>
