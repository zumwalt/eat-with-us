<header class="banner" role="banner">
  <div class="container first-header">
    <div class='row'>

    <?php
      // start blog conditional
      global $blog_id;
      if($blog_id == 6):
    ?>

      <?php
        $placement = get_field('logo_placement','option');
      ?>

      <?php if( $placement === 'left' ): ?>

        <div class="brand_wrapper large-4 small-4 columns">
          <a class="brand" href="<?php echo home_url(); ?>/"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/harveys/harveys-logo.png"></a>
        </div>

        <div class="large-4 small-4 columns extra"></div>

        <div class="large-4 small-4 columns promo right<?php if(get_field('disable_promo_area_margin','options')): ?> flush<?php endif; ?>">

          <?php if(get_field('header_image', 'option')): ?>
          <a href="<?php the_field('link', 'option'); ?>">
            <?php $image = get_field('header_image', 'option'); ?>
            <img src="<?php echo $image ?>" />
          </a>
          <?php endif; ?>

        </div>

      <?php elseif($placement === 'middle'): ?>

        
          <div class="large-4 small-4 columns extra">

          </div>

          <div class="brand_wrapper large-4 small-4 columns">
            <a class="brand" href="<?php echo home_url(); ?>/"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/harveys/harveys-logo.png"></a>
          </div>

          <div class="large-4 small-4 columns extra"></div>
        

      <?php elseif($placement === 'right'): ?>

        <div class="large-4  small-4 columns promo left">

          <?php if(get_field('header_image', 'option')): ?>
          <a href="<?php the_field('link', 'option'); ?>">
            <?php $image = get_field('header_image', 'option'); ?>
            <img src="<?php echo $image ?>" />
          </a>
          <?php endif; ?>

        </div>

        <div class="large-4 small-4 columns extra"></div>

        <div class="large-4 small-4 columns">
          <a class="brand" href="<?php echo home_url(); ?>/"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/harveys/harveys-logo.png"></a>
        </div>

      <?php endif; ?>

    <?php else: ?>

      <?php
        $placement = get_field('logo_placement','option');
      ?>

      <?php if( $placement === 'left' ): ?>

        <div class="brand_wrapper large-5 small-12 columns">
          <a class="brand" href="<?php echo home_url(); ?>/"><?php bloginfo('name'); ?></a>
        </div>

        <div class="large-4 offset-by-3  small-12 columns promo right<?php if(get_field('disable_promo_area_margin','options')): ?> flush<?php endif; ?>">

          <?php if(get_field('header_image', 'option')): ?>
          <a href="<?php the_field('link', 'option'); ?>">
            <?php $image = get_field('header_image', 'option'); ?>
            <img src="<?php echo $image ?>" />
          </a>
          <?php endif; ?>

        </div>

      <?php elseif($placement === 'middle'): ?>

        
          <div class="masthead row">
            <a href="/"></a>
          </div>
        

      <?php elseif($placement === 'right'): ?>

        <div class="large-4  small-6 columns promo left">

          <?php if(get_field('header_image', 'option')): ?>
          <a href="<?php the_field('link', 'option'); ?>">
            <?php $image = get_field('header_image', 'option'); ?>
            <img src="<?php echo $image ?>" />
          </a>
          <?php endif; ?>

        </div>

        <div class="large-5 offset-by-3  small-6 columns">
          <a class="brand" href="<?php echo home_url(); ?>/"><?php bloginfo('name'); ?></a>
        </div>

      <?php endif; ?>

    <?php endif; // blog conditional ?>


    </div>
  </div> 

  <div class="container second-header">
    <nav class="nav-main row<?php if( $placement === 'right' ): echo' left'; endif; ?>" role="navigation">
        <?php
          if (has_nav_menu('primary_navigation')) :
            wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav small-12 large-12 columns'));
          endif;
        ?>
      </nav>
  </div>
</header>