<?php dynamic_sidebar('sidebar-secondary'); ?>




	<h4 class="title">Recent Posts</h4>
	<ul class="recent_news">
     <?php
	 	global $post;
		$myposts = get_posts();
		foreach($myposts as $post) : 
			setup_postdata($post);
	 ?>

	 <li>
		<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
	    <time>Posted on <?php the_time('M j Y'); ?></time>
	 </li>

	<?php endforeach; ?>
    </ul>