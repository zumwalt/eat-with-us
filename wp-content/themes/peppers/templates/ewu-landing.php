  <header class="banner">
        <div class="container">
            <div class="logo">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ewu/logo-ewu.png">
            </div>   
        </div>
        <div class="shadow">
            
        </div>
    </header>
    <div class="content">
        <div class="container">
            <ul class="brands">
                <li class="brand"><a class="peppers switched_images" href="http://eatwithpeppers.com"><img class="des" src="<?php echo get_template_directory_uri(); ?>/assets/img/ewu/logo-peppers.png"></a></li>
                <li class="brand"><a class="harveys switched_images" href="http://eatwithharveys.com"><img class="des" src="<?php echo get_template_directory_uri(); ?>/assets/img/ewu/logo-harveys.png"></a></li>
                <li class="brand"><a class="grill switched_images" href="http://eatwiththegrill.com"><img class="des" src="<?php echo get_template_directory_uri(); ?>/assets/img/ewu/logo-grill.png"></a></li>
            </ul>
        </div>
        
    </div>
    <footer>
        <div class="container">
            <div class="contact">
                <ul>
                  <li>Eat With Us Group</li>
                  <li>P.O. Box 1368&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;      Columbus, MS 39703</li>
                  <li>TOLL-FREE: (888) 222.9550</li>
                  <li>PHONE: (662) 327.6982</li>
                  <li>FAX: (662) 327.1672</li>
                </ul>
                <p>&copy; Eat With Us All Rights Reserved</p>

            </div>
        </div>
        
    </footer>

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-41468606-1', 'eatwithus.com');
      ga('send', 'pageview');

    </script>