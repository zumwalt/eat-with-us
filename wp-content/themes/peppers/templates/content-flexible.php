<section class="flexible-spaces">
<?php 
  $flex = get_field("home_content");
  $class = "container";
  while(the_flexible_field("home_content")):
  if(get_sub_field("featured"))

  ?>
				<?php if(get_row_layout() == "menu_slider"): ?>
				
				<div id="menu_slider" class="<?php echo $class; ?>">
					<div class="row">
						<div id="menu-carousel" class="large-12 columns flexslider">
						<ul class="slides" >

  						<?php while(has_sub_field('menu_category')) :?>

  							<?php $image_id = get_sub_field('menu_cat_image'); ?>
  								
  							
                  <li class='slide'>
                    <a href="<?php the_sub_field('menu_cat_link'); ?>">
                  <div class="image-wrapper">
                    <?php echo wp_get_attachment_image( $image_id, $size = 'thumbnail', $icon = false, $attr = '' ); ?>
                  </div>
  								<h6><?php the_sub_field('menu_cat_title'); ?></h6>
                  </a>
  							</li>
                

  						<?php endwhile; ?>

						</ul>
						</div>
					</div>
				</div>
				<?php endif; ?>

        
        <?php if(get_row_layout() == "image_description"): ?>
           	<div id="image_description" class="<?php echo $class; ?> two_col">
          		<div class="row">

            		<?php                                                          
                    $image_id = get_sub_field('image_1');
                    $image_array = wp_get_attachment_image_src($image_id, $size='full', $icon = false );
                  	$content = get_sub_field('content');
                   	$orientation = get_sub_field('left_or_right');
                   	$title = get_sub_field('title');
                ?>

         				<?php if($orientation === 'left'): ?>

           				<div id="left-two-col" class="large-12 columns">

             				<div class="large-6 columns image-holder text-center">
                      	<div class="wrap">
                      	<img src='<?php echo $image_array[0];  ?>' />
                      	</div>
                    </div>

                    <div class="large-6 columns text-area">
                    	<h2><?php echo $title ?></h2>
                    	<p><?php echo $content ?></p>
                    </div>

                 	</div>

              <?php else: ?>
                  
                  <div id="left-two-col" class="large-12 columns">

                    <div class="large-6 columns text-area">
                      <h2><?php echo $title ?></h2>
                      <p><?php echo $content ?></p>
                    </div>

                    <div class="large-6 columns image-holder text-center">
                        <div class="wrap">
                        <img src='<?php echo $image_array[0];  ?>' />
                        </div>
                    </div>

                  </div>

              <?php endif; ?>
                        
              </div>
        	</div>
                        
 
      <?php endif; ?>


        <?php if(get_row_layout() === "three_featured_items"): ?>
            <div id="three_feature" class="<?php echo $class; ?> three_feature">

          		<div class="row">
              	<div class="large-10 small-12 large-centered columns">
                  <h3 class="featured"><?php the_sub_field('featured_section_title'); ?></h2>
              	<?php while(has_sub_field('featured_item')): ?>

              		<?php $image_id = get_sub_field('image_repeat'); ?>
              		<div class="feature large-4 small-4 columns">
              			<div class="img_wrap"><?php echo wp_get_attachment_image( $image_id, $size = 'full', $icon = false, $attr = '' ); ?></div>
              			<h4><?php the_sub_field('title_repeat'); ?></h4>
              			<p><?php the_sub_field('desc_repeat'); ?></p>
              		</div>
              	<?php endwhile; ?>
              	</div>
              </div>
            </div>

        <?php endif; ?>

        <?php if(get_row_layout() === "divider"): ?>
          <div class="row">
            <hr />
          </div>
        <?php endif; ?>

        <?php if(get_row_layout() === "mailing_list_promo"): ?>
          <div class="mailing-list <?php echo $class; ?>">
            <div class="row">
              <div class="large-5 small-12 columns">
                <?php $mail_image = get_sub_field('mail_image'); ?>
                <div class="mail-image"><?php echo wp_get_attachment_image( $mail_image, $size = 'full', $icon = false, $attr = '' ); ?></div>
              </div>
              <div class="large-6 small-12 columns text">
                <h2><?php the_sub_field('title'); ?></h2>
                <p><?php the_sub_field('content'); ?></p>
                <p class="mailer_link"><a class="button red" href="<?php the_sub_field('mailer_link'); ?>"><?php the_sub_field('mailer_link_text');?></a></p>
              </div>
              
            </div>
          </div>
        <?php endif; ?>

        


<?php endwhile; ?>
</section>
