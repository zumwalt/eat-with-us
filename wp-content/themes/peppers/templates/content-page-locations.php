<?php while (have_posts()) : the_post(); ?>
  <?php the_content(); ?>
  <ul class="locations">
  <?php
  // The Query
  $args = array(
      "post_type" => "location"
    );
  $the_query = new WP_Query( $args );

  // The Loop
  while ( $the_query->have_posts() ) :
    $the_query->the_post(); ?>

    <li class="location small-6 columns">
      <a href="<?php the_permalink(); ?>">
        <div class="image">
          <?php
          $attachment_id = get_field('image');
          $size = "menu_item"; // (thumbnail, medium, large, full or custom size)
           
          $image = wp_get_attachment_image_src( $attachment_id, $size );
          // url = $image[0];
          // width = $image[1];
          // height = $image[2];
          ?>
          <img src="<?php echo $image[0]; ?>" />
        </div>
        <div class="information">
          <h4><?php the_title(); ?></h4>
          <span class="address">
            <?php the_field('street_address'); ?><br/>
            <?php the_field('city_state_zip'); ?><br/>
          </span>
          <span class="phone">
            <b>p</b> <?php the_field('phone'); ?>
          </span>
          <span class="phone">
            <b>f</b> <?php the_field('fax'); ?>
          </span>
        </div>
      </a>
    </li>

  <?php endwhile;
  /* Restore original Post Data 
   * NB: Because we are using new WP_Query we aren't stomping on the 
   * original $wp_query and it does not need to be reset.
  */ ?>
  </ul>
  <?php wp_link_pages(array('before' => '<nav class="pagination">', 'after' => '</nav>')); ?>
<?php endwhile; ?>