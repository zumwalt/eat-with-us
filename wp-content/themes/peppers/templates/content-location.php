<?php
 $street = get_field("street_address");
 $city = get_field("city_state_zip");
 ?>


<script type="text/javascript">
	
$(document).ready(function() {
  

	var location = <?php echo json_encode($street . ', ' . $city, JSON_HEX_TAG); ?>

      var map = new GMaps({
        div: '#map',
        lat: -12.043333,
        lng: -77.028333,
        draggable: false,

      });
      
	GMaps.geocode({
  address: location,
  callback: function(results, status) {
    if (status == 'OK') {
      var latlng = results[0].geometry.location;
      map.setCenter(latlng.lat(), latlng.lng());
      map.addMarker({
        lat: latlng.lat(),
        lng: latlng.lng()
      });
    }
  }
});
	});	
</script>
<div class="container">
  <div class="row location-wrap">
    <div id="map" class="large-12 columns"></div>
    <div id="details" class="location large-12 columns">
      <div class="contact row">
        <div class="title large-4 columns">
          <h4><?php echo get_the_title(); ?></h4>
          <span class="general-address"><?php the_field('general_address'); ?> </span>
          <span class="manager"><?php the_field('manager'); ?></span>
        </div>
        <div class="information large-4 columns">
            <span class="address"><?php the_field('street_address'); ?><br />
                <?php the_field('city_state_zip'); ?></span>
            <span class="phone"><b>p</b> <?php the_field('phone'); ?></span>
            <span class="fax"><b>f</b> <?php the_field('fax'); ?></span>
            <span class="hours"><?php the_field('store_hours'); ?></span>
        </div>
        <div class="social large-4 columns hide-for-small">
          <ul class="social-ul">
            <?php while(has_sub_field('social_links')): ?>

              <li><a href="<?php the_sub_field('link_href'); ?>" target="_blank"><span class="img"><img src="<?php the_sub_field('icon'); ?>" /></span><span class="link"><?php the_sub_field('link_text'); ?></span></a></li>
            <?php endwhile; ?>
          </ul>
        </div>
        <div class="large-4 columns">
            <div class="large-4 columns">
              <a href=""><span></span></a>
            </div>
        </div>
      </div>
      <hr>
      <div class="additional-info row">
        <div class="image large-4 columns">
          <?php
          $attachment_id = get_field('image');
          $size = "menu_item"; // (thumbnail, medium, large, full or custom size)
           
          $image = wp_get_attachment_image_src( $attachment_id, $size );
          // url = $image[0];
          // width = $image[1];
          // height = $image[2];
          ?>
          <img src="<?php echo $image[0]; ?>" />
        </div>
        <div class="news large-8 columns">
          <h4>Store News</h4>
          <ul class="recent-news">
            <?php
            global $blog_id;
            $user = get_field('user');
            $user_id = $user['ID'];
            $super_user = 34;
            $user_array = " . $user_id . , . $super_user .";

            if ($user):
               $args = array(
                  "posts_per_page" => 2,
                  'author' =>  $user_array 
                );
             
            else:
               $args = array(
                "posts_per_page" => 2,
                'author' => $super_user
               );
             
            endif;

            $my_query = new WP_Query( $args );
               if ( $my_query->have_posts() ) { 
                   while ( $my_query->have_posts() ) { 
                       $my_query->the_post(); ?>
            
                        <li>
                          <h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                          <time>Posted on <?php the_time('M j Y'); ?></time>
                          <p><?php echo excerpt(15); ?></p>
                          <a class="read-more" href="<?php the_permalink(); ?>">Read More</a>
                        </li>

            <?php }
            }
            wp_reset_postdata(); ?>  
                    
            

            
            </ul> 
        </div>
        <div class="row social-mobile show-for-small">
          <div class="social small-12 small-centered columns">
            <hr />
          <ul class="social-ul large-block-grid-3">
            <li><a class="ss-icon ss-social-circle" href="<?php the_field('twitter_link'); ?>" target="_blank">Twitter</a></li>
            <li><a class="ss-icon ss-social-circle" href="#" target="_blank">Facebook</a></li>
            <li><a class="ss-icon ss-social-circle" href="#" target="_blank">Email</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
