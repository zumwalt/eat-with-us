<?php
/*
Template Name: Menu Template
*/
?>

<?php if(is_page("Menu")): ?>
  <script>
  window.location.replace("/menu/featured-items/");
  </script>
<?php endif; ?>

<div class="page-header">
   <?php wp_nav_menu(array(
      "menu" => "Menu Navigation",
      "menu_class" => "menu-categories"
   )); ?>
</div>

<?php while (have_posts()) : the_post(); ?>
  <?php the_content(); ?>
  
    <?php 
      $posts = get_field('menu_items');
       
      if( $posts ): ?>
        <div class="menu-items">

        <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
          <?php setup_postdata($post); ?>
            <div class="menu-item">

              <?php 
                $attachment_id = get_field('image');
                if($attachment_id):
                $size = "menu_item"; // (thumbnail, medium, large, full or custom size)
                 
                $image = wp_get_attachment_image_src( $attachment_id, $size );
                // url = $image[0];
                // width = $image[1];
                // height = $image[2];
              ?>
              <img src="<?php echo $image[0]; ?>" />
              <?php endif; ?>
              <h5 class="menu-item-title"><?php the_title(); ?></h5>
              <?php the_content(); ?>

            </div>
        <?php endforeach; ?>

        </div>

        <script type="text/javascript">
          <?php 
            global $blog_id;
            if ($blog_id == 6) { 
          ?>

            $(window).load(function(){
              // Menu Masonry
              // -------------------------

              $('.menu-items').masonry({
                itemSelector: '.menu-item'
              });

              $(window).resize(function(){
                if( $(window).width() < 480 ) {
                  $('.menu-items').masonry({
                    itemSelector: '.menu-item',
                    // set columnWidth a fraction of the container width
                    columnWidth: function( containerWidth ) {
                      return containerWidth / 1;
                    }
                  });
                }
              });
            });

          <?php  } else { ?>

            $(window).load(function(){
              // Menu Masonry
              // -------------------------

              $('.menu-items').masonry({
                itemSelector: '.menu-item'
              });

              $(window).resize(function(){
                if( $(window).width() < 480 ) {
                  $('.menu-items').masonry({
                    itemSelector: '.menu-item',
                    // set columnWidth a fraction of the container width
                    columnWidth: function( containerWidth ) {
                      return containerWidth / 1;
                    }
                  });
                }
              });
            });

          <?php  } ?>
        </script>
        <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
      <?php endif; ?>

    
  <?php wp_link_pages(array('before' => '<nav class="pagination">', 'after' => '</nav>')); ?>
<?php endwhile; ?>