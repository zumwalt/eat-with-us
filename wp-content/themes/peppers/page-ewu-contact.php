<?php
/*
Template Name: EWU Contact
*/
?>

<div class="page-header">
  <h1>
    <?php 
      switch_to_blog(1);
      the_field('contact_title', 'option');
      restore_current_blog();
    ?>
  </h1>
</div>


<?php while (have_posts()) : the_post(); ?>
  <?php 
    switch_to_blog(1);
    the_field('contact_content', 'option');
    restore_current_blog();
  ?>
<?php endwhile; ?>