

<div class="page-header">
   <?php wp_nav_menu(array(
      "menu" => "Menu Navigation",
      "menu_class" => "menu-categories"
   )); ?>
</div>

<?php while (have_posts()) : the_post(); ?>
        
        <li class="menu-item">
          <?php 
            $attachment_id = get_field('image');
            $size = "menu_item"; // (thumbnail, medium, large, full or custom size)
             
            $image = wp_get_attachment_image_src( $attachment_id, $size );
            // url = $image[0];
            // width = $image[1];
            // height = $image[2];
          ?>
          <img src="<?php echo $image[0]; ?>" />
          <h5 class="menu-item-title"><?php the_title(); ?></h5>
          <?php the_content(); ?>
        </li>



  <?php wp_link_pages(array('before' => '<nav class="pagination">', 'after' => '</nav>')); ?>
  <?php endwhile; ?>