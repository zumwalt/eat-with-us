<?php
/**
 * Roots includes
 */
require_once locate_template('/lib/utils.php');           // Utility functions
require_once locate_template('/lib/init.php');            // Initial theme setup and constants
require_once locate_template('/lib/sidebar.php');         // Sidebar class
require_once locate_template('/lib/config.php');          // Configuration
require_once locate_template('/lib/activation.php');      // Theme activation
require_once locate_template('/lib/cleanup.php');         // Cleanup
require_once locate_template('/lib/nav.php');             // Custom nav modifications
require_once locate_template('/lib/comments.php');        // Custom comments modifications
require_once locate_template('/lib/rewrites.php');        // URL rewriting for assets
require_once locate_template('/lib/htaccess.php');        // HTML5 Boilerplate .htaccess
require_once locate_template('/lib/widgets.php');         // Sidebars and widgets
require_once locate_template('/lib/scripts.php');         // Scripts and stylesheets
require_once locate_template('/lib/custom.php');          // Custom functions
require_once locate_template('/lib/mobile-nav.php');      // Custom mobile Nav walker

// Post Thumbnails
// -------------------------

if ( function_exists( 'add_theme_support' ) ) {
  add_theme_support( 'post-thumbnails' );
        set_post_thumbnail_size( 150, 150 ); // default Post Thumbnail dimensions   
}

if ( function_exists( 'add_image_size' ) ) { 
  //add_image_size( 'category-thumb', 300, 9999 ); //300 pixels wide (and unlimited height)
  add_image_size( 'menu_item', 238, 148, true ); //(cropped)
}


/*ACF Categories registration */

if (function_exists('register_field')) { 
  register_field('Categories_field', dirname(__File__) . '/categories.php'); 
}


// Register Custom Post Type
function location_post() {
	$labels = array(
		'name'                => _x( 'Locations', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Location', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Locations', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Location:', 'text_domain' ),
		'all_items'           => __( 'All Locations', 'text_domain' ),
		'view_item'           => __( 'View Location', 'text_domain' ),
		'add_new_item'        => __( 'Add New Location', 'text_domain' ),
		'add_new'             => __( 'New Location', 'text_domain' ),
		'edit_item'           => __( 'Edit Location', 'text_domain' ),
		'update_item'         => __( 'Update Location', 'text_domain' ),
		'search_items'        => __( 'Search locations', 'text_domain' ),
		'not_found'           => __( 'No locations found', 'text_domain' ),
		'not_found_in_trash'  => __( 'No locations found in Trash', 'text_domain' ),
	);

	$args = array(
		'label'               => __( 'location', 'text_domain' ),
		'description'         => __( 'Location information pages', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', ),
		'taxonomies'          => array( 'states' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => '',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);

	register_post_type( 'location', $args );
}

add_action( 'init', 'register_taxonomy_states' );

function register_taxonomy_states() {

    $labels = array( 
        'name' => _x( 'States', 'states' ),
        'singular_name' => _x( 'State', 'states' ),
        'search_items' => _x( 'Search States', 'states' ),
        'popular_items' => _x( 'Popular States', 'states' ),
        'all_items' => _x( 'All States', 'states' ),
        'parent_item' => _x( 'Parent State', 'states' ),
        'parent_item_colon' => _x( 'Parent State:', 'states' ),
        'edit_item' => _x( 'Edit State', 'states' ),
        'update_item' => _x( 'Update State', 'states' ),
        'add_new_item' => _x( 'Add New State', 'states' ),
        'new_item_name' => _x( 'New State', 'states' ),
        'separate_items_with_commas' => _x( 'Separate states with commas', 'states' ),
        'add_or_remove_items' => _x( 'Add or remove States', 'states' ),
        'choose_from_most_used' => _x( 'Choose from most used States', 'states' ),
        'menu_name' => _x( 'States', 'states' ),
    );

    $args = array( 
        'labels' => $labels,
        'public' => true,
        'show_in_nav_menus' => true,
        'show_ui' => true,
        'show_tagcloud' => true,
        'show_admin_column' => false,
        'hierarchical' => false,

        'rewrite' => true,
        'query_var' => true
    );

    register_taxonomy( 'states', array('location'), $args );
}

add_action( 'init', 'register_cpt_menu_item' );

function register_cpt_menu_item() {

    $labels = array( 
        'name' => _x( 'Menu Items', 'menu_item' ),
        'singular_name' => _x( 'Menu Item', 'menu_item' ),
        'add_new' => _x( 'Add New', 'menu_item' ),
        'add_new_item' => _x( 'Add New Menu Item', 'menu_item' ),
        'edit_item' => _x( 'Edit Menu Item', 'menu_item' ),
        'new_item' => _x( 'New Menu Item', 'menu_item' ),
        'view_item' => _x( 'View Menu Item', 'menu_item' ),
        'search_items' => _x( 'Search Menu Items', 'menu_item' ),
        'not_found' => _x( 'No menu items found', 'menu_item' ),
        'not_found_in_trash' => _x( 'No menu items found in Trash', 'menu_item' ),
        'parent_item_colon' => _x( 'Parent Menu Item:', 'menu_item' ),
        'menu_name' => _x( 'Menu Items', 'menu_item' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        
        'supports' => array( 'title', 'editor' ),
        'taxonomies' => array( 'menu_categories' ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );

    register_post_type( 'menu_item', $args );
}

add_action( 'init', 'register_taxonomy_menu_categories' );

function register_taxonomy_menu_categories() {

    $labels = array( 
        'name' => _x( 'Menu Categories', 'menu_categories' ),
        'singular_name' => _x( 'Menu Category', 'menu_categories' ),
        'search_items' => _x( 'Search Menu Categories', 'menu_categories' ),
        'popular_items' => _x( 'Popular Menu Categories', 'menu_categories' ),
        'all_items' => _x( 'All Menu Categories', 'menu_categories' ),
        'parent_item' => _x( 'Parent Menu Category', 'menu_categories' ),
        'parent_item_colon' => _x( 'Parent Menu Category:', 'menu_categories' ),
        'edit_item' => _x( 'Edit Menu Category', 'menu_categories' ),
        'update_item' => _x( 'Update Menu Category', 'menu_categories' ),
        'add_new_item' => _x( 'Add New Menu Category', 'menu_categories' ),
        'new_item_name' => _x( 'New Menu Category', 'menu_categories' ),
        'separate_items_with_commas' => _x( 'Separate menu categories with commas', 'menu_categories' ),
        'add_or_remove_items' => _x( 'Add or remove Menu Categories', 'menu_categories' ),
        'choose_from_most_used' => _x( 'Choose from most used Menu Categories', 'menu_categories' ),
        'menu_name' => _x( 'Menu Categories', 'menu_categories' ),
    );

    $args = array( 
        'labels' => $labels,
        'public' => true,
        'show_in_nav_menus' => true,
        'show_ui' => true,
        'show_tagcloud' => true,
        'show_admin_column' => false,
        'hierarchical' => false,

        'rewrite' => true,
        'query_var' => true
    );

    register_taxonomy( 'menu_categories', array('menu_item'), $args );
}

// Hook into the 'init' action
add_action( 'init', 'location_post', 0 );

 include_once( WP_PLUGIN_DIR . '/advanced-custom-fields-location-field-add-on/location-field.php' );


 if(function_exists("register_options_page"))
{
    register_options_page('Header');
    register_options_page('Sidebar');
    register_options_page('Footer');
 }

 function excerpt($limit) {
       $excerpt = explode(' ', get_the_excerpt(), $limit);
       if (count($excerpt)>=$limit) {
         array_pop($excerpt);
         $excerpt = implode(" ",$excerpt).'...';
       } else {
         $excerpt = implode(" ",$excerpt);
       } 
       $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
       return $excerpt;
     }

     function content($limit) {
       $content = explode(' ', get_the_content(), $limit);
       if (count($content)>=$limit) {
         array_pop($content);
         $content = implode(" ",$content).'...';
       } else {
         $content = implode(" ",$content);
       } 
       $content = preg_replace('/\[.+\]/','', $content);
       $content = apply_filters('the_content', $content); 
       $content = str_replace(']]>', ']]&gt;', $content);
       return $content;
     }

     // Apply filter
     add_filter('body_class', 'multisite_body_classes');

     function multisite_body_classes($classes) {
             $id = get_current_blog_id();
             $slug = strtolower(str_replace(' ', '-', trim(get_bloginfo('name'))));
             $classes[] = $slug;
             $classes[] = 'site-id-'.$id;
             return $classes;
     }

     // Custom CSS for the whole admin area
     // Create wp-admin.css in your theme folder
     function wpfme_adminCSS() {
      echo '<link rel="stylesheet" type="text/css" href="'.get_bloginfo('template_directory').'/assets/css/wp-admin.css"/>';
     }
     add_action('admin_head', 'wpfme_adminCSS');



     function my_custom_fonts() {
       global $blog_id;
       if($blog_id == 4) {
       echo '
         <script type="text/javascript" src="//use.typekit.net/yaf1wtr.js"></script>
         <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
       ';
       } elseif($blog_id == 5) {
        echo '
         <script type="text/javascript" src="//use.typekit.net/mau0knr.js"></script>
         <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
        ';
       } elseif($blog_id == 6) {
        echo '
         <script type="text/javascript" src="//use.typekit.net/uew4rws.js"></script>
         <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
        ';
       }
     }
     add_action('admin_head', 'my_custom_fonts');

     function is_blog () {
        global  $post;
        $posttype = get_post_type($post );
        return ( ((is_archive()) || (is_author()) || (is_category()) || (is_home()) || (is_single()) || (is_tag())) && ( $posttype == 'post')  ) ? true : false ;
     }


?>