<?php
/*
Template Name: Location Template
*/
?>

<?php if(is_page("Locations")): ?>
  <script>
  window.location.replace("/locations/all/");
  </script>
<?php endif; ?>

<div class="page-header">
   <?php wp_nav_menu(array(
      "menu" => "Location Navigation",
      "menu_class" => "location-categories"
   )); ?>
</div>

<?php while (have_posts()) : the_post(); ?>
  <?php the_content(); ?>
  <div class="location-items">
    <?php 
      $posts = get_field('choose_locations');
       
      if( $posts ): ?>
        <ul class="locations">
        <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
          <?php setup_postdata($post); ?>
            <li class="location small-6 columns">
              <a href="<?php the_permalink(); ?>">
                <div class="image">
                  <?php
                  $attachment_id = get_field('image');
                  $size = "menu_item"; // (thumbnail, medium, large, full or custom size)
                   
                  $image = wp_get_attachment_image_src( $attachment_id, $size );
                  // url = $image[0];
                  // width = $image[1];
                  // height = $image[2];
                  ?>
                  <img src="<?php echo $image[0]; ?>" />
                </div>
                <div class="information">
                  <h4><?php the_title(); ?></h4>
                  <span class="address">
                    <?php the_field('street_address'); ?><br/>
                    <?php the_field('city_state_zip'); ?><br/>
                  </span>
                  <span class="phone">
                    <b>p</b> <?php the_field('phone'); ?>
                  </span>
                  <span class="phone">
                    <b>f</b> <?php the_field('fax'); ?>
                  </span>
                </div>
              </a>
            </li>
        <?php endforeach; ?>
        </ul>
        <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
      <?php endif; ?>
    </div>
  <?php wp_link_pages(array('before' => '<nav class="pagination">', 'after' => '</nav>')); ?>
<?php endwhile; ?>