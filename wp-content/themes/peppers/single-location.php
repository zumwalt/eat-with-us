<?php
 $street = get_field("street_address");
 $city = get_field("city_state_zip");
 ?>


<script type="text/javascript">
	
$(document).ready(function() {
  

	var location = <?php echo json_encode($street . ', ' . $city, JSON_HEX_TAG); ?>


    
      map = new GMaps({
        div: '#map',
        lat: -12.043333,
        lng: -77.028333,
        height: 500,
        width: 600
      });
      
	GMaps.geocode({
  address: location,
  callback: function(results, status) {
    if (status == 'OK') {
      var latlng = results[0].geometry.location;
      map.setCenter(latlng.lat(), latlng.lng());
      map.addMarker({
        lat: latlng.lat(),
        lng: latlng.lng()
      });
    }
  }
});
	});	
</script>

<div id="map"></div>